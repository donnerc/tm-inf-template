# Template de base pour les TMs d'informatique du CSUD

## Installation

### Prérequis sur Cloud9

Sur Cloud9, l'installation est relativement facile. Pour pouvoir exécuter Sphinx et LaTeX2e, il est nécessaire de créer une workspace Cloud9 **de type Python**. 

### Prérequis sur votre machine locale

Il faut commencer par installer Python 3 sur la machine pour pouvoir exécuter les commandes suivantes. De plus, les commandes suivantes doivent être installées

* `pip`
* `virtualenv`

Sur le bash Linux, on peut le faire avec

```{bash}
sudo apt-get install python3-pip python3-venv
```

### Installation de Sphinx

Pour installer ce template Sphinx, il suffit de se trouver dans un terminal Linux disposant de la command `git` et de faire

```{bash}
# Cloner le dépôt
git clone https://donnerc@bitbucket.org/donnerc/tm-inf-template.git
cd tm-inf-template

# Création de l'environnement virtuel
virtualenv -p python3 venv

# Activer l'environnement virtual Python
source venv/bin/activate

# Installation de Sphinx et autres dépendances
pip install -r requirements.txt
```

# Configuration de Sphinx

Il faut insérer vos informations (auteur, titre, ... ) dans le fichier `source/conf.py`. Les modifications à faire sont présentées dans le gist https://gist.github.com/donnerc/8d4a7aea8c40fb62541a203eab08bab6/revisions.

## Compilation en HTML

Pour compiler les fichiers Sphinx en HTML, il faut entrer la commande suivante depuis le dossier racine du projet contenant le fichier `Makefile` :

```{bash}
make livehtml
```

## Visualisation des pages HTML générées

### Depuis une machine de développement locale

Il suffit ensuite, pour visualiser le résultat, de visiter http://localhost:8080/ dans le navigateur Web.

### Avec Cloud9

Il faut visiter l'url affichée dans le terminal Cloud9 lors de la compilation (`https://sphinx2018-donnerc.c9users.io/`) dans l'exemple ci-dessous :

```
(venv)donnerc:~/workspace/tm-inf-template (master) $ make livehtml 
Serving pages on https://sphinx2018-donnerc.c9users.io/
sphinx-autobuild -b html -d build/doctrees   source build/html --port=8080 --host=0.0.0.0 > /dev/null
WARNING: The config value `latex_show_urls' has type `bool', defaults to `str'.
[I 171207 06:45:10 server:283] Serving on http://0.0.0.0:8080
[I 171207 06:45:10 handlers:60] Start watching changes
[I 171207 06:45:10 handlers:62] Start detecting changes
```

Il est également possible de lancer la page avec le menu *Preview > Preview Running Application*. 

## Génération du PDF 

Pour pouvoir générer le PDF, il faut au préalable installer LaTeX sur le système. Sur un système Linux, il suffit de faire les commandes

```{bash}
sudo apt-get install texlive-latex-extra texlive-lang-french texlive-fonts-recommended latexmk
```

Sur les machines Windows, l'installation risque d'être plus compliquée, raison pour laquelle il est conseillé de compiler votre travail directement dans Cloud9 ou dans le Bash Ubuntu de Windows 10.

Ensuite, pour générer le PDF, il suffit de faire depuis la racine du dépôt où se trouve le fichier `Makefile` principal :

```{bash}
make tmpdf
```

et de se rendre dans le dossier `build/latex/` et ouvrir le fichier `tm-ecrit.pdf` généré par LaTeX.

## Faire un fork du dépôt

Il peut être pratique de faire un fork du dépôt https://bitbucket.org/donnerc/tm-inf-template avant de le cloner sur votre machine. Cela vous permettra de travailler dans votre propre copier et de pouvoir modifier le dépôt git.


# Ressources

Se référer aux dépôts suivants qui ont été largement utilisés :

* https://github.com/GaretJax/sphinx-autobuild (LiveReload de la documentation)

