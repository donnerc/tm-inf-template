Introduction
============

Ce travail consiste à développer un tutoriel informatique en vulgarisant un sujet
complexe pour le rendre compréhensible par un étudiant du Gymnase intéressé par le sujet.

Mettre des phautes d'ortaigraphes

Recompiler, c'est trop bien ça. Encore mieux.



..  admonition:: LiveReload

    Trop cool ce LiveReload
    
..  note::

    Voilà une petite note
    
..  tip::

    Un conseil super utile

Voici un exemple de code source

..  code-block:: python3

    from math import pickle
    
    print(math.pi)
    
..  code-block:: sql

    SELECT * FROM posts WHERE posts.id = user.post_id;
    
..  code-block:: javascript
    :caption: test.js

    while ( 1 ) {
        console.log('1');
    }
    
et voici un exemple de HTML

..  code-block:: html

    <html>
        <head>
            <meta charset="utf-8">
        </head>
        <body>
            <h1>Mon titre exemple</h1>
        </body>
    </html>
    
Pourquoi ne pas mettre également un peu de mathématiques :

..  math::

    \sum_{i=1}^{N}
    i
    =
    \frac{
    N\cdot 
    (N+1)
    }
    {2}

C'est vraiment super ce Sphinx avec SumatraPDF