class TMConfig:

    title = u'titre du TM (à modifier)'
    author = u'Auteur du travail (à modifier)'
    year = u'2019'
    month = u'Janvier'
    seminary_title = u'Projet de robotique et Internet des objets'
    tutor = u"Cédric Donner"

    @classmethod
    def date(cls):
        return cls.month + " " + cls.year